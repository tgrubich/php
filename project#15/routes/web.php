<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/first', array('as' => 'index','uses' => 'TestController@getFirstTask'));

Route::get('/second', array('as' => 'index','uses' => 'TestController@getSecondTask'));

Route::get('/third', array('as' => 'index','uses' => 'TestController@getThirdTask'));

Route::get('/four', array('as' => 'index','uses' => 'TestController@getFourTask'));

Route::get('/five', array('as' => 'index','uses' => 'TestController@getFiveTask'));