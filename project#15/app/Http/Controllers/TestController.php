<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function getFirstTask()
    {
        echo "<br />" . "<b>First task: </b>" . "<br />";

        $users = DB::table('workers')
            ->select('*', 'series', 'number')
            ->where('series', 419148)
            ->where('number', 4367)
            ->get();
        foreach ($users as $join) {
            echo "<b>First name: </b>" . $join->first_name . " ";
            echo "<b>Last name: </b>" . $join->last_name . " ";
            echo "<b>Company: </b>" . $join->company . " ";
            echo "<b>Role: </b>" . $join->role . " ";
            echo "<b>Number: </b>" . $join->number . " ";
            echo "<b>Series: </b>" . $join->series . " ";
            echo "<b>Birthday: </b>" . $join->birthday . "<br />";
        }
    }

    public function getSecondTask()
    {
        echo "<br />" . "<b>Second task: </b>" . "<br />";

        $date = DB::table('workers')
            ->select('*', 'last_name', 'birthday')
            ->where('birthday', '>', '1999-07-01')
            ->get();
        foreach ($date as $join) {
            echo "<b>First name: </b>" . $join->first_name . " ";
            echo "<b>Last name: </b>" . $join->last_name . " ";
            echo "<b>Company: </b>" . $join->company . " ";
            echo "<b>Role: </b>" . $join->role . " ";
            echo "<b>Number: </b>" . $join->number . " ";
            echo "<b>Series: </b>" . $join->series . " ";
            echo "<b>Birthday: </b>" . $join->birthday . "<br />";
        }
    }

    public function getThirdTask()
    {
        echo "<br />" . "<b>Third task: </b>" . "<br />";

        $count = DB::table('workers')
            ->select('*', 'company')
            ->where('company', 'Северсталь')
            ->count();
        echo "<b>Count: </b>" . $count . "<br />";
    }

    public function getFourTask()
    {
        echo "<br />" . "<b>Four task: </b>" . "<br />";

        $staff = DB::table('workers')
            ->select('*', 'last_name', 'first_name', 'role', 'birthday')
            ->where('birthday', '>', '1979-12-01')
            ->where('birthday', '<', '1982-02-01')
            ->where('role', 'Инженер')
            ->orderByRaw('first_name')
            ->orderByRaw('last_name')
            ->get();
        foreach ($staff as $join) {
            echo "<b>First name: </b>" . $join->first_name . " ";
            echo "<b>Last name: </b>" . $join->last_name . " ";
            echo "<b>Company: </b>" . $join->company . " ";
            echo "<b>Role: </b>" . $join->role . " ";
            echo "<b>Number: </b>" . $join->number . " ";
            echo "<b>Series: </b>" . $join->series . " ";
            echo "<b>Birthday: </b>" . $join->birthday . "<br />";
        }
    }

    public function getFiveTask()
    {
        echo "<br />" . "<b>Five task: </b>" . "<br />";

        $one = 'Химик-технолог';
        $two = 'Химик';
        $three = 'Биохимик';

        $role = DB::table('workers')
            ->select('*', 'company', 'role', 'birthday')
            ->where('company','X5 Retail Group')
            ->where(function($query) use ($one, $two, $three){
                $query->where('role', 'LIKE', '%'.$one.'%')
                    ->orWhere('role', 'LIKE', '%'.$two.'%')
                    ->orWhere('role', 'LIKE', '%'.$three.'%');
            })
            ->orderByDesc('birthday')
            ->get();
        foreach ($role as $join) {
            echo "<b>First name: </b>" . $join->first_name . " ";
            echo "<b>Last name: </b>" . $join->last_name . " ";
            echo "<b>Company: </b>" . $join->company . " ";
            echo "<b>Role: </b>" . $join->role . " ";
            echo "<b>Number: </b>" . $join->number . " ";
            echo "<b>Series: </b>" . $join->series . " ";
            echo "<b>Birthday: </b>" . $join->birthday . "<br />";
        }
    }
}