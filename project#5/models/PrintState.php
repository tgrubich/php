<?php

abstract class PrintState
{
    abstract protected function getState();

    public function print()
    {
        print $this->getState();
    }
}