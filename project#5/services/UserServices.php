<?php

interface UserServices
{
    public function addUserLoginPassword($login, $password);

    public function findByLogin($login, $password);
}