Проект #9 (стажировка Hawking School)

Задание:

- установить Laravel на локальном компьютере;
- запустить локальный сервер разработки Laravel artisan;
- изменить главную страницу приложения (на своё усмотрение, хоть Hello World).

Первым шагом при установке Laravel была сделана проверка
наличия установленного Composer.
![Image text](resources/1.png)
И выполнена команда composer.
![Image text](resources/2.png)
Далее пошагово проведена установка Laravel:
- composer create-project laravel/laravel example-app: создание проекта
с названием ecample-app
![Image text](resources/3.png)
- cd example-app: переход в папку с проектом
![Image text](resources/4.png)
- php artisan serve – запуск локального сервера разработки arcisan
![Image text](resources/5.png)

Для изменения домашней страницы Laravel были внесены корректировки в файл
welcome.blade.php
![Image text](resources/6.png)
Итоговый вид главной страницы Laravel
![Image text](resources/7.png)