<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialObjectType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'material',
    ];

    public function realestate()
    {
        return $this->belongsTo(Realestate::class);
    }

    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'material_object_type';
}