<?php

namespace App\Http\Controllers;

use App\Compare;
use App\Favorite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

class TestController
{

    public function getItemDetails($id)
    {
        echo "<br />" . "<b>Item Details: </b>" . "<br />";
        $joins = DB::table('realestate')
            ->join('object_type', 'realestate.id', '=', 'object_type.realestate_id')
            ->join('house_state', 'realestate.id', '=', 'house_state.realestate_id')
            ->join('house_type', 'realestate.id', '=', 'house_type.realestate_id')
            ->join('material_object_type', 'realestate.id', '=', 'material_object_type.realestate_id')
            ->join('my_user', 'realestate.id', '=', 'my_user.realestate_id')
            ->select('realestate.*', 'object_type.types', 'house_state.state', 'house_type.type', 'material_object_type.material', 'my_user.last_name')
            ->where('realestate.id', '=', $id)
            ->get();
        foreach ($joins as $join) {
            echo '******************************************';
            echo "<br />";
            echo "<b>Living space: </b>" . $join->living_space . "<br />";
            echo "<b>Room`s number: </b>" . $join->room_number . "<br />";
            echo "<b>Floor: </b>" . $join->floor . "<br />";
            echo "<b>Address: </b>" . $join->address . "<br />";
            echo "<b>Ceiling height: </b>" . $join->ceiling_height . "<br />";
            echo "<b>Floor`s level: </b>" . $join->level_floor . "<br />";
            echo "<b>City area: </b>" . $join->city_area . "<br />";
            echo "<b>Charactristics: </b>" . $join->characteristics . "<br />";
            echo "<b>Object type: </b>" . $join->types . "<br />";
            echo "<b>House state: </b>" . $join->state . "<br />";
            echo "<b>House type: </b>" . $join->type . "<br />";
            echo "<b>Material type: </b>" . $join->material . "<br />";
            echo "<b>User: </b>" . $join->last_name . "<br />";
        }
    }

    public function itemDetails($id): \Illuminate\Support\Collection
    {
        echo "<br />" . "<b>Item Details: </b>" . "<br />";
        $item = DB::table('realestate')
            ->join('object_type', 'realestate.id', '=', 'object_type.realestate_id')
            ->join('house_state', 'realestate.id', '=', 'house_state.realestate_id')
            ->join('house_type', 'realestate.id', '=', 'house_type.realestate_id')
            ->join('material_object_type', 'realestate.id', '=', 'material_object_type.realestate_id')
            ->join('my_user', 'realestate.id', '=', 'my_user.realestate_id')
            ->select('realestate.*', 'object_type.types', 'house_state.state', 'house_type.type', 'material_object_type.material', 'my_user.last_name')
            ->where('realestate.id', '=', $id)
            ->get();
        return $item;
    }

    public function addCompareItems($id1, $id2)
    {
        echo "<br />" . "<b>Objects: </b>" . "<br />";
        $check = DB::table('compare')->get();
        if ($check->count() != 2) {
            $first = $this->itemDetails($id1)->toJson();
            DB::insert('insert into compare (object) values(?)', [$first]);
            $second = $this->itemDetails($id2)->toJson();
            DB::insert('insert into compare (object) values(?)', [$second]);
        }

        $joins = DB::table('compare')->get()->toArray();
        foreach ($joins as $join) {
            echo '******************************************';
            echo "<br />";
            echo "<b>Object: </b>" . $join->object . "<br />";
        }
    }

    public function hbCompareItems()
    {
        $first = $this->itemDetails(1)->toJson();
        $response = Http::withBody(
            base64_encode($first), 'application/json'
        )->post('https://adresat.pro/compare/');
        return $response;
    }

    public function deleteCompareItems(): \Illuminate\Http\RedirectResponse
    {
        Compare::getQuery()->delete();
        return redirect('/');
    }

    public function addFavoriteItems($id1)
    {
        echo "<br />" . "<b>Objects: </b>" . "<br />";
        $first = $this->itemDetails($id1)->toJson();
        DB::insert('insert into favorites (object) values(?)', [$first]);

        $joins = DB::table('favorites')->get()->toArray();
        foreach ($joins as $join) {
            echo '******************************************';
            echo "<br />";
            echo "<b>Object: </b>" . $join->object . "<br />";
        }
    }

    public function showFavoriteItems()
    {
        echo "<br />" . "<b>Objects: </b>" . "<br />";
        $joins = DB::table('favorites')->get()->toArray();
        foreach ($joins as $join) {
            echo '******************************************';
            echo "<br />";
            echo "<b>Object: </b>" . $join->object . "<br />";
        }
    }

    public function deleteAllFavoriteItems(): \Illuminate\Http\RedirectResponse
    {
        Favorite::getQuery()->delete();
        return redirect('/');
    }

    public function deleteFavoriteItemsById($id): \Illuminate\Http\RedirectResponse
    {
        Favorite::getQuery()->delete($id);
        return redirect('/');
    }
}