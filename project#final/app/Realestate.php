<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Realestate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'living_space', 'room_number', 'floor', 'address', 'ceiling_height', 'level_floor'
, 'city_area', 'characteristics', 'cost'    ];

    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'realestate';
}