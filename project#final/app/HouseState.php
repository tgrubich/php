<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseState extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state',
    ];

    public function realestate()
    {
        return $this->belongsTo(Realestate::class);
    }

    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'house_state';
}