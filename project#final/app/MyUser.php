<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'phone'
    ];

    public function realestate()
    {
        return $this->belongsTo(Realestate::class);
    }

    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'my_user';
}