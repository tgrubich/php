<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'types',
    ];

    public function realestate()
    {
        return $this->belongsTo(Realestate::class);
    }

    /**
     * Таблица, связанная с моделью.
     *
     * @var string
     */
    protected $table = 'object_type';
}