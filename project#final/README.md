Курсовой проект (стажировка Hawking School)

Задание:

Необходимо реализовать Rest Api для отдачи json для части функционала сайта
https://adresat.pro/

Что нужно реализовать:
- роут отдающий деталку товара https://adresat.pro/catalog/sale-rooms/281035
- роуты необходимые для помещения товара в сравнение и удаление товара из
сравнения
- роуты необходимые для помещения товара в избранное и удаление товара из
избранного
- роут отдачи данных для страницы сравнения https://adresat.pro/compare/
- *роут отдачи данных для страницы избранного

При выполнении курсовой работы, чем больше возможностей Laravel и дополнительных
сервисов будет использовано, тем лучше. Например:
- данные для деталки товара (описание товара, данные ипотеки) можно хранить в
кэше
- данные для сравнения/избранного (товары и их характеристики) можно хранить в
ElasticSearch
- список товаров в сравнении/избранном можно хранить в куках, а можно в БД
- при использовании ElasticSearch можно создать периодическое событие
обновления индексов
- изображения для деталки товара получать с помощью Storage
- реализовать авторизацию через токен с использованием Laravel Passport,
чтобы по токену получать сравнение/избранное для конкретного пользователя

Таким образом любые технологии пройденные в рамках занятий могут быть
применены в рамках курсовой работы. И чем больше практики, тем лучше результат!

Для реализации задания курсовой работы была изучена информация на сайте
https://adresat.pro/
На основе изученной нформации сформированы миграции и созданы таблицы базы данных
Каждая таблица заполнена рандомными данными при помощи сидов и фабрик
![Image text](redmeimage/1.png)

Для получение деталки товара в контроллере TestController реализован метод
public function getItemDetails($id)
![Image text](redmeimage/2.png)
В данный метод передается id товараи выводятся все совпадения товара по id
В курасовой работе, в связи с рандомным заполнением данными связанных таблиц
могут возникать ситуации, когда выводится более одной записи. Это связано с тем,
что у связанных таблиц может несколько раз появляться id основной таблицы и 
соответственно будут получены объекты с одинаковым id, но разными характеристиками

Также реализован роут, через который осуществляется обращение к методу контроллера
и получение деталки товара
![Image text](redmeimage/3.png)
Результат работы метода в случае когда id товара встречается в связанных таблицах
один раз
![Image text](redmeimage/4.png)
Результат работы метода, когда id товара встречается несколько раз в связанных
таблицах
![Image text](redmeimage/5.png)

Для сравнения товаров создана таблица compare. Чтобы добавить товары для сравнения,
необхожимо передать id двух товаров в метод public function addCompareItems($id1, $id2)
![Image text](redmeimage/6.png)
В контроллере реализован роут
![Image text](redmeimage/7.png)
Результат работы метода
![Image text](redmeimage/8.png)
![Image text](redmeimage/9.png)

Для удаления данных из таблица сравнения товара реализован метод
public function deleteCompareItems()
![Image text](redmeimage/10.png)
После удаления данных из таблицы пользователь перенаправляется на главную
страницу
Роут для реализации очищения данных таблицы сравнений
![Image text](redmeimage/11.png)

Для сохранения избранных товаров создана таблица Favorites
![Image text](redmeimage/12.png)
Реализованы роуты для добавления товара в избранное, просмотра товаров, находящихся
в избранном и удаления товаров из избранного. Удаление можно делать
по id и полное очищение данных таблицы избранных товаров
![Image text](redmeimage/13.png)
Методы которые вызываются при срабатывании роутеров:
<b>public function addFavoriteItems($id1)</b>
![Image text](redmeimage/14.png)
<b>public function showFavoriteItems()</b>
![Image text](redmeimage/15.png)
<b>public function deleteAllFavoriteItems()</b>
![Image text](redmeimage/16.png)
<b>public function deleteFavoriteItemsById($id)</b>
![Image text](redmeimage/17.png)

Результат работы метода добавления товара в таблицу Избранное:
![Image text](redmeimage/18.png)
Из-за заполнения данных таблиц рандомно и тестовыми данными получается
ситуация, когда под одним id находятся объекты недвижимости с некоторыми
отличиями в таких полях, как тип объекта недвижимости и так далее

Результат работы метода вывода данных из таблицы Избранное:
![Image text](redmeimage/19.png)

При удалении данных из таблицы Избранное данные удаляются и пользователь
перенаправляется на главную страницу приложения

Для отдачи даных из таблицы сравнения был реализован метод
![Image text](redmeimage/20.png)
Также реализован роут
![Image text](redmeimage/21.png)