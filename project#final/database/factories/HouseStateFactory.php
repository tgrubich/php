<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HouseState;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(HouseState::class, function (Faker $faker) {

    $arr = ['без отделки', 'требуется капитальный ремонт', 'чистовая отделка', 'требуется косметический ремонт', 'хороший ремонт', 'евроремонт', 'дизайнерский ремонт'];
    return [
        'state' => Arr::random($arr),
        'realestate_id' => rand(1, 50),
    ];
});