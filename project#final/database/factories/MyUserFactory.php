<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MyUser;
use Faker\Generator as Faker;

$factory->define(MyUser::class, function (Faker $faker) {

    return [
        'last_name' => $faker->lastName,
        'first_name' => $faker->firstName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'realestate_id' => rand(1, 50),
    ];
});