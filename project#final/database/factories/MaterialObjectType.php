<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MaterialObjectType;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(MaterialObjectType::class, function (Faker $faker) {

    $arr = ['блок', 'панель', 'кирпич'];
    return [
        'material' => Arr::random($arr),
        'realestate_id' => rand(1, 50),
    ];
});