<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HouseType;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(HouseType::class, function (Faker $faker) {

    $arr = ['новостройка', 'вторичное жилье'];
    return [
        'type' => Arr::random($arr),
        'realestate_id' => rand(1, 50),
    ];
});