<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ObjectType;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(ObjectType::class, function (Faker $faker) {

    $arr = ['гостинка', 'малосемейка', 'сталинка', 'хрущевка', 'брежневка', 'студия', 'стандартная квартира', 'лофт', 'частный дом', 'коттедж', 'вилла', 'дуплекс', 'таунхаус', 'пентхаус', 'апартаменты', 'атриумариум'];
    return [
        'types' => Arr::random($arr),
        'realestate_id' => rand(1, 50),
    ];
});