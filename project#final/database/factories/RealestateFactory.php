<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Realestate;
use Faker\Generator as Faker;

$factory->define(Realestate::class, function (Faker $faker) {

    return [
        'living_space' => $faker->numberBetween(18.0, 200.0),
        'room_number' => $faker->numberBetween(1, 5),
        'floor' => $faker->numberBetween(5, 50),
        'address' => $faker->address,
        'ceiling_height' => $faker->numberBetween(1.80, 3.0),
        'level_floor' => $faker->numberBetween(1, 50),
        'city_area' => $faker->citySuffix,
        'characteristics' => $faker->sentence,
        'cost' => $faker->numberBetween(850, 20000000),
    ];
});