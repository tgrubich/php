<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeighKeyHouseState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('house_state', function (Blueprint $table) {
            $table->integer('realestate_id')->unsigned()->index();
            $table->foreign('realestate_id')->references('id')->on('realestate')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('house_state', function (Blueprint $table) {
            $table->dropColumn("realestate_id");
        });
    }
}
