<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealestateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realestate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('living_space', 5)->nullable(false);
            $table->string('room_number', 2)->nullable(false);
            $table->string('floor', 10)->nullable(false);
            $table->string('address', 255)->nullable(false);
            $table->string('ceiling_height', 5)->nullable(false);
            $table->string('level_floor', 3)->nullable(false);
            $table->string('city_area', 255)->nullable(false);
            $table->string('characteristics', 255)->nullable(false);
            $table->double('cost')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realestate');
    }
}
