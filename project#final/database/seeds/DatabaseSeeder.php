<?php

use App\HouseState;
use App\HouseType;
use App\MaterialObjectType;
use App\MyUser;
use App\ObjectType;
use App\Realestate;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Realestate::class, 50)->create();

        factory(ObjectType::class, 50)->create();

        factory(MyUser::class, 50)->create();

        factory(HouseType::class, 50)->create();

        factory(HouseState::class, 50)->create();

        factory(MaterialObjectType::class, 50)->create();
    }
}
