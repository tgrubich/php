<?php

use Illuminate\Database\Seeder;

class HouseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\HouseType::class)->create()->each(function($u) {
            $u->realestate()->save(factory(App\Realestate::class)->make());
        });
    }
}
