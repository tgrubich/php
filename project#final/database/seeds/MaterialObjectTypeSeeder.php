<?php

use Illuminate\Database\Seeder;

class MaterialObjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MaterialObjectType::class)->create()->each(function($u) {
            $u->realestate()->save(factory(App\Realestate::class)->make());
        });
    }
}
