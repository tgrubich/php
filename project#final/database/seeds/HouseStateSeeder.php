<?php

use Illuminate\Database\Seeder;

class HouseStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\HouseState::class)->create()->each(function($u) {
            $u->realestate()->save(factory(App\Realestate::class)->make());
        });
    }
}
