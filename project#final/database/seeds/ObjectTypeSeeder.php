<?php

use Illuminate\Database\Seeder;

class ObjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ObjectType::class)->create()->each(function($u) {
            $u->realestate()->save(factory(App\Realestate::class)->make());
        });
    }
}
