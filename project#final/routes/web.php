<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/item/{id}', [TestController::class, 'getItemDetails']);

Route::any('/compare/{id1}/{id2}', [TestController::class, 'addCompareItems']);

Route::any('/compare/delete', [TestController::class, 'deleteCompareItems']);

Route::get('/compare/hb',  [TestController::class, 'hbCompareItems']);

Route::any('/favorite/{id1}', [TestController::class, 'addFavoriteItems']);

Route::any('/favorite', [TestController::class, 'showFavoriteItems']);

Route::any('/favorite/delete/all', [TestController::class, 'deleteAllFavoriteItems']);

Route::any('/favorite/delete/{id}', [TestController::class, 'deleteFavoriteItemsById']);