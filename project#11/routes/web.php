<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::name('post.')->group(function (){

    Route::view('/create', 'create')->name('create');
//
//    Route::get('/login', function (){
//        if (Auth::check()){
//            return redirect(route('users.private'));
//        }
//        return view('login');
//    })->name('login');
//
//    Route::post('/login', [LoginController::class, 'login']);
//
//    Route::get('/logout', function (){
//        Auth::logout();
//        return redirect('/');
//    })->name('logout');

    Route::get('/create', function (){
        if (Auth::check()){
            return redirect(route('post.create'));
        }
        return view('create');
    })->name('create');

    Route::post('/create', [PostController::class, 'create']);
});
