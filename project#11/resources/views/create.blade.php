<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}}">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
</head>
<body>
<h1>Create a post</h1>

<form class="col-3 offset-4 border rounded" method="POST" action="{{route('post.create')}}">
    @csrf
    <div class="form-group">
        <label for="title" class="col-form-label-lg">Title</label>
        <input class="form-control" id="title" name="title" type="text" value="" placeholder="Title">
        @error('title')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="text" class="col-form-label-lg">Text</label>
        <input class="form-control" id="text" name="text" type="text" value="" placeholder="text">
        @error('text')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-primary" type="submit" name="sendMe" value="1">Submit</button>
    </div>
</form>
</body>
</html>
