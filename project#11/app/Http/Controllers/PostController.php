<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;

class PostController extends Controller
{
    public function create(Request $request){

        $validateFields = $request->validate([
            'title' => 'required',
            'text' => 'required'
        ]);

        if (Post::where('title', $validateFields['title'])->exists()) {
            return redirect(route('post.create'))->withErrors([
                'title' => 'This title name is already exists'
            ]);
        }

        $post = Post::create($validateFields);
        if ($post) {
            return redirect()->to(route('post.create'));
        }
        return redirect(route('post.create'))->withErrors([
            'formError' => 'Error saving post'
        ]);

    }
}
