<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inner', array('as' => 'index','uses' => 'TestController@getInnerJoin'));

Route::get('/left', array('as' => 'index','uses' => 'TestController@getLeftJoin'));

Route::get('/right', array('as' => 'index','uses' => 'TestController@getRightJoin'));

Route::get('/cross', array('as' => 'index','uses' => 'TestController@getCrossJoin'));
