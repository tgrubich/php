<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class TestController extends Controller
{

    public function getInnerJoin()
    {
        echo "<br />"."<b>INNER JOIN: </b>"."<br />";
        $joins = DB::table('my_users')
            ->join('phones', 'my_users.id', '=', 'phones.my_user_id')
            ->join('posts', 'my_users.id', '=', 'posts.my_user_id')
            ->select('my_users.*', 'phones.phone', 'posts.text')
            ->get();
        foreach ($joins as $join) {
            echo "<b>User: </b>".$join->name." ";
            echo "<b>Phone: </b>".$join->phone." ";
            echo "<b>Text: </b>".$join->text."<br />";
        }
    }

    public function getLeftJoin()
    {
        echo "<br />"."<b>LEFT JOIN: </b>"."<br />";
        $users = DB::table('my_users')
            ->leftJoin('posts', 'my_users.id', '=', 'posts.my_user_id')
            ->get();
        foreach ($users as $user) {
            echo "<b>User: </b>".$user->name . " ";
            echo "<b>Text: </b>".$user->text . "<br />";
        }
    }

    public function getRightJoin()
    {
        echo "<br />"."<b>RIGHT JOIN: </b>"."<br />";
        $users = DB::table('my_users')
            ->rightJoin('phones', 'my_users.id', '=', 'phones.my_user_id')
            ->get();
        foreach ($users as $user) {
            echo "<b>User: </b>".$user->name . " ";
            echo "<b>Phone: </b>".$user->phone . "<br />";
        }
    }

    public function getCrossJoin()
    {
        echo "<br />"."<b>CROSS JOIN: </b>"."<br />";
        $sizes = DB::table('my_users')
            ->crossJoin('posts')
            ->get();
        foreach ($sizes as $size) {
            echo "<b>User: </b>".$size->name . " ";
            echo "<b>Post: </b>".$size->title . "<br />";
        }
    }


}
