Проект #14 (стажировка Hawking School)

Задание:

- привести примеры использования Join в проекте
- связать ранее созданные на проекте модели
- для получения связанных записей использовать жадную загрузку
- вынести в трейты методы, которые могут быть использованы не только
конкретной моделью, в которую они добавлены

В проекте реализованы четыре вида джоинов. Для каждого джоина реализован
метод в классе TestController

<b>public function getInnerJoin()</b>
![Image text](redmeimage/1.png)
Результат выполнения метода
![Image text](redmeimage/2.png)

<b>public function getLeftJoin()</b>
![Image text](redmeimage/4.png)
Результат выполнения метода
![Image text](redmeimage/3.png)

<b>public function getRightJoin()</b>
![Image text](redmeimage/5.png)
Результат выполнения метода
![Image text](redmeimage/6.png)

<b>public function getCrossJoin()</b>
![Image text](redmeimage/7.png)
Результат выполнения метода
![Image text](redmeimage/8.png)

Для отображения результатов выполнения методов были реализованы роуты
![Image text](redmeimage/9.png)
