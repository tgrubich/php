<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    $x = 0;
    $number = $x + 1;

    return [
        'title' => $faker->title,
        'text' =>$faker->text,
        'my_user_id' => rand(1, 50),
    ];
});
