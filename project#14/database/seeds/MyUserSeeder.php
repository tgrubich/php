<?php

use Illuminate\Database\Seeder;

class MyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MyUser::class)->create();
    }
}
