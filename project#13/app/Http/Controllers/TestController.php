<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class TestController extends Controller
{

    public function getUserList()
    {
        $users = DB::table('my_users')->get();
        foreach ($users as $user) {
            echo "<b>Name: </b>".$user->name." ";
            echo "<b>Email: </b>".$user->email."<br />";
        }
    }

    public function getPostsList()
    {
        $posts = DB::table('posts')->get();
        foreach ($posts as $post) {
            echo "<b>Title: </b>".$post->title." ";
            echo "<b>Text: </b>".$post->text." ";
            $user = $this->getUserById($post->my_user_id);
            echo "<b>User: </b>".$user->name."<br />";
        }
    }

    public function getPhonesList()
    {
        $phones = DB::table('phones')->get();
        foreach ($phones as $phon) {
            echo "<b>Phone: </b>".$phon->phone." ";
            $user = $this->getUserById($phon->my_user_id);
            echo "<b>User: </b>".$user->name."<br />";
        }
    }

    public function getUserById($id){
        return DB::table('my_users')->find($id);
    }

    public function queryAllTables()
    {
        echo "<b>Find user by name: </b>"."<br />";
        $user = DB::table('my_users')->where('name', 'Myles Metz')->first();
        echo $user->name."<br />";

        echo "<b>Users count: </b>"."<br />";
        $users = DB::table('my_users')->count();
        echo $users."<br />";

        echo "<b>First post: </b>"."<br />";
        $posts = DB::table('posts')
            ->select('title', 'my_user_id')
            ->get()->first();
        foreach ($posts as $post) {
            echo "<b>Title: </b>".$post." ";
        }


        echo "<br />"."<b>JOIN: </b>"."<br />";
        $joins = DB::table('my_users')
            ->join('phones', 'my_users.id', '=', 'phones.my_user_id')
            ->join('posts', 'my_users.id', '=', 'posts.my_user_id')
            ->select('my_users.*', 'phones.phone', 'posts.text')
            ->get();
        foreach ($joins as $join) {
            echo $join->name." ";
            echo $join->phone." ";
            echo $join->text."<br />";
        }
    }

}
