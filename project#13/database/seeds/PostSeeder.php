<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class)->create()->each(function($u) {
            $u->user()->save(factory(App\MyUser::class)->make());
        });
    }
}
