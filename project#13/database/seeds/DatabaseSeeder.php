<?php

use App\MyUser;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(MyUser::class, 50)->create();

        factory(App\Phone::class, 50)->create();

        factory(App\Post::class, 50)->create();
    }
}
