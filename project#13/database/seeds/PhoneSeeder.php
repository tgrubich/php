<?php

use Illuminate\Database\Seeder;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Phone::class)->create()->each(function($u) {
            $u->user()->save(factory(App\MyUser::class)->make());
        });
    }
}
