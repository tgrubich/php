<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Phone;
use Faker\Generator as Faker;

$factory->define(Phone::class, function (Faker $faker) {
    $x = 0;
    $number = $x + 1;
    return [
        'phone' => $faker->phoneNumber,
        'my_user_id' => rand(1, 50),
    ];
});
