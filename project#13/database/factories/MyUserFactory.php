<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MyUser;
use Faker\Generator as Faker;

$factory->define(MyUser::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});
