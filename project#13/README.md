Проект #13 (стажировка Hawking School)

Задание:

- создать при помощи консольных команд модели для необходимых на своем
проекте таблиц
- в моделях написать методы, которые продемонстрируют работу с Eloquent
или построителем запросов (использовать то, что больше нравится),
например, получить записи, дата создания которых не больше 2 часов от
текущего момента времени; найти запись таблицы по одному из параметров
(name, title) кроме id
- реализовать часть методов получения данных из БД с использованием
коллекций

Для примера реализации связи "один-к-одному" созданы модели MyUser и
Phone, также созданы соответствующие миграции
![Image text](redmeimage/1.png)
Таким образом, одному пользователю будет соответствовать только один
номер телефона.

Для примера реализации связи "один-ко-многим" созданы модель Post и
миграция
![Image text](redmeimage/2.png)
Один пользователь может иметь много постов.

Для корректного создания связей между таблицами были созданы отдельные
миграции для реализации связей по вторичным ключам

Для таблицы phones
![Image text](redmeimage/4.png)

Для таблицы posts
![Image text](redmeimage/5.png)

Для заполнения таблиц рандомными значениями реализованы сиды и фабрики.
Фабрика для таблицы my_users
![Image text](redmeimage/3.png)
Фабрика для таблицы phones
![Image text](redmeimage/6.png)
Фабрика для таблицы posts
![Image text](redmeimage/7.png)
Сид для таблицы my_users
![Image text](redmeimage/8.png)
Сид для таблицы phones
![Image text](redmeimage/9.png)
Сид для таблицы posts
![Image text](redmeimage/10.png)

Созданные объекты вызываются в DatabaseSeeder
![Image text](redmeimage/11.png)

Далее были запущены сиды и заполнена база данных
![Image text](redmeimage/12.png)
Таблица my_users
![Image text](redmeimage/13.png)
Таблица posts
![Image text](redmeimage/14.png)
Таблица phones
![Image text](redmeimage/15.png)

Реализованы операции по выборке данных из таблиц
В каждую модель была добавлена protected переменная в которой хранится
название таблицы с которой связана модель. Создан класс TestController
в котором реализованы различные выборки данных.

<b>public function getUserList()</b>
![Image text](redmeimage/16.png)
Позволяет вывести на экран всех пользователей, которые находятся в
таблице my_users
![Image text](redmeimage/17.png)

<b>public function getPhonesList()</b>
![Image text](redmeimage/20.png)
Позволяет вывести на экран все телефоны, которые находятся в
таблице phones
![Image text](redmeimage/19.png)

<b>public function getPostsList()</b>
![Image text](redmeimage/18.png)
Позволяет вывести на экран все посты, которые находятся в
таблице posts
![Image text](redmeimage/21.png)

<b>public function queryAllTables()</b>
![Image text](redmeimage/22.png)
Метод для выборки данных из трех таблиц
![Image text](redmeimage/23.png)

Для отображения результатов запросов были реализованы роуты
![Image text](redmeimage/24.png)
