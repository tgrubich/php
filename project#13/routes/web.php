<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', array('as' => 'index','uses' => 'TestController@getUserList'));

Route::get('/posts', array('as' => 'index','uses' => 'TestController@getPostsList'));

Route::get('/phones', array('as' => 'index','uses' => 'TestController@getPhonesList'));

Route::get('/all', array('as' => 'index','uses' => 'TestController@queryAllTables'));
