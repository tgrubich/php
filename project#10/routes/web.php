<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'Hello! I am tasting Laravel`s routs!! This is an example of a simple "get" request';
});

Route::any('/any', function () {
    return 'At the address of "any" I can perform any type of request';
});

Route::match(['get', 'post'], '/match', function () {
    return 'Hi, everybody!!!';
});

Route::redirect('/any', '/', 301);

Route::permanentRedirect('/any', '/');

Route::view('/all', 'all');

Route::get('/user/{id?}/{name?}', function ($id = null, $name = null)
{
    return 'You indicated this id: '.$id.' Name: '.$name;
})->where('id', '[0-9]+')->where('name', '[A-Za-z]+')->name('user');

Route::get('/date/{date?}', function ($date = null)
{
    return 'You date is: '.$date;
})->name('date');

Route::view('/test', 'test');

Route::get('/redirect/{id?}/{name?}', function ($id = null, $name = null){
   return redirect()->route('user', ['id' => $id, 'name' => $name]);
});

Route::name('users.')->group(function (){

    Route::view('/private', 'private')->middleware('auth')->name('private');

    Route::get('/login', function (){
        if (Auth::check()){
            return redirect(route('users.private'));
        }
        return view('login');
    })->name('login');

    Route::post('/login', [LoginController::class, 'login']);

    Route::get('/logout', function (){
        Auth::logout();
        return redirect('/');
    })->name('logout');

    Route::get('/registration', function (){
        if (Auth::check()){
            return redirect(route('users.private'));
        }
        return view('registration');
    })->name('registration');

    Route::post('/registration', [RegisterController::class, 'save']);
});
