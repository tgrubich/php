<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
</head>
<body>
<h1>Регистрация</h1>

<form class="col-3 offset-4 border rounded" method="POST" action="{{route('users.registration')}}">
    @csrf
    <div class="form-group">
        <label for="email" class="col-form-label-lg">Email</label>
        <input class="form-control" id="email" name="email" type="text" value="" placeholder="Email">
        @error('email')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="password" class="col-form-label-lg">Password</label>
        <input class="form-control" id="password" name="password" type="password" value="" placeholder="Password">
        @error('password')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-primary" type="submit" name="sendMe" value="1">Enter</button>
    </div>
</form>

</body>
</html>
