<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="description"/>
    <meta name="keywords" content="keywords"/>
    <meta name="author" content="author"/>
    <title>Colorvoid Website Template (demo)</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="ie_fixes.css" media="screen" /><![endif]-->
</head>
<body>
<div id="layout_wrapper">
    <div id="layout_edgetop"></div>
    <div id="layout_container">
        <div id="site_title">
            <h1 class="left"><a href="http://elevenproject.local/all">Test page</a></h1>
            <h2 class="right">This page was created to test a simple query that returns a page to display the result</h2>
            <div class="clearer">&nbsp;</div>
        </div>
    </div>
</div>
</div>
</body>
</html>
