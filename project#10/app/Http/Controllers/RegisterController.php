<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function save(Request $request)
    {

        if (Auth::check())
            return redirect()->to(route('users.private'));

        $validateFields = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (User::where('email', $validateFields['email'])->exists()) {
            return redirect(route('users.registration'))->withErrors([
                'email' => 'This user already exists'
            ]);
        }

        $user = User::create($validateFields);
        if ($user) {
            Auth::login($user);
            return redirect()->to(route('users.private'));
        }
        return redirect(route('users.login'))->withErrors([
            'formError' => 'Error saving user'
        ]);
    }
}
