<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request){
        if (Auth::check())
            return redirect()->intended(route('users.private'));

        $fromFields = $request->only(['email', 'password']);

        if (Auth::attempt($fromFields)){
            return redirect()->intended(route('users.private'));
        }

        return redirect(route('users.login'))->withErrors([
           'email' => 'Failed to login'
        ]);
    }
}
