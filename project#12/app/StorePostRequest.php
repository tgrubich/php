<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class StorePostRequest extends Model
{
    public function uniquePostValidation($validateFields){
        if (Post::where('title', $validateFields['title'])->exists()) {
            return -1;
        }
        else
            return $validateFields;
    }

    public function requestValidation(Request $request){
        $validateFields = $request->validate([
            'title' => 'required|unique:posts',
            'text' => 'required',
            'image' => 'required'
        ]);
        return $validateFields;
    }
}
