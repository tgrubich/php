<?php

namespace App\Http\Controllers;

use App\Post;
use App\StorePostRequest;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create(Request $request){

        $vlidate = new StorePostRequest();

        $validateFields = $vlidate->requestValidation($request);

        $test = $vlidate->uniquePostValidation($validateFields);
        if ($test == -1)
            return redirect(route('post.create'))->withErrors([
                'title' => 'This title name is already exists'
            ]);

        $post = Post::create($validateFields);
        if ($post) {
            return redirect()->to(route('post.create'));
        }
        return redirect(route('post.create'))->withErrors([
            'formError' => 'Error saving post'
        ]);

    }
}
