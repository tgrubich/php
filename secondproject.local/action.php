<HTML>
<HEAD>
    <TITLE>Result</TITLE>
</HEAD>
<BODY background="https://thumbs.dreamstime.com/b/%D1%80%D0%B0%D0%B7%D0%BD%D0%BE%D1%86%D0%B2%D0%B5%D1%82%D0%BD%D1%8B%D0%B9-%D0%B2%D0%B7%D1%80%D1%8B%D0%B2-%D0%BF%D0%BE%D1%80%D0%BE%D1%85%D0%B0-%D0%BD%D0%B0-%D0%B1%D0%B5%D0%BB%D0%BE%D0%BC-%D1%84%D0%BE%D0%BD%D0%B5-%D0%B0%D0%B1%D1%81%D1%82%D1%80%D0%B0%D0%BA%D1%82%D0%BD%D0%B0%D1%8F-%D0%BF%D0%B0%D1%81%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BF%D1%8B%D0%BB%D1%8C-162178629.jpg">
<h1 align="center">Result</h1>
<h1 style="text-align: center; size: 16px; color: blue">Your choice:</h1>
<h1 style="text-align: center">
<?php
echo $_GET['image'];
?>
</h1>
<br>
<br>
<br>

<input type="text" id="search" name="search" placeholder="Enter userid">
<input type="button" value="Search" id="but_search">
<br/>
<input type="button" value="Fetch all records" id="but_fetchall">

<table border='1' id='userTable' style='border-collapse: collapse;'>
    <thead>
    <tr>
        <th>ID</th>
        <th>Login</th>
        <th>Password</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<?php
//session_start();
$db = pg_connect("host=localhost port=5433 dbname=secondDB user=postgres password=000");

$request = "";
if(isset($_POST['request'])){
  $request = $_POST['request'];
}

// Fetch all records
if($request == 'fetchall'){

  $query = "SELECT * FROM users";

  $result = pg_query($db, $query);

  $response = array();

  while ($row = pg_fetch_assoc($result) ){

     $id = $row['id'];
     $login = $row['login'];
     $password = $row['password'];

     $response[] = array(
        "id" => $id,
        "login" => $login,
        "password" => $password,
     );
  }

  echo json_encode($response);
  die;
}

// Fetch record by id
if($request == 'fetchbyid'){

  $userid = 0;
  if(isset($_POST['id']) && is_numeric($_POST['id']) ){
    $userid = $_POST['id'];
  }

  $query = "SELECT * FROM users WHERE id='$userid'";
  $result = pg_query($db, $query);

  $response = array();
  if (pg_numrows($result) > 0) {

    $row = pg_fetch_assoc($result);

    $id = $row['id'];
    $login = $row['login'];
    $password = $row['password'];

    $response[] = array(
      "id" => $id,
      "login" => $login,
      "password" => $password,
    );
  }

  echo json_encode($response);
  die;
}
?>

<script>
    $(document).ready(function(){

        // Fetch all records
        $('#but_fetchall').click(function(){

            // AJAX GET request
            $.ajax({
                url: 'action.php',
                type: 'post',
                data: {request: 'fetchall'},
                dataType: 'json',
                success: function(response){

                    createRows(response);

                }
            });
        });

        // Search by userid
        $('#but_search').click(function(){
            var userid = Number($('#search').val().trim());
            if(!isNaN(userid)){
                userid = Number(userid);
            }else{
                userid = 0;
            }
            if(userid > 0){
                // AJAX POST request
                $.ajax({
                    url: 'action.php',
                    type: 'post',
                    data: {request: 'fetchbyid', userid: userid},
                    dataType: 'json',
                    success: function(response){
                        createRows(response);
                    }
                });
            }

        });

    });

    // Create table rows
    function createRows(response){
        var len = 0;
        $('#userTable tbody').empty(); // Empty <tbody>
        if(response != null){
            len = response.length;
        }

        if(len > 0){
            for(var i=0; i<len; i++){
                var id = response[i].id;
                var login = response[i].login;
                var password = response[i].password;

                var tr_str = "<tr>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'>" + login + "</td>" +
                    "<td align='center'>" + password + "</td>" +
                    "</tr>";

                $("#userTable tbody").append(tr_str);
            }
        }else{
            var tr_str = "<tr>" +
                "<td align='center' colspan='4'>No record found.</td>" +
                "</tr>";

            $("#userTable tbody").append(tr_str);
        }
    }
</script>


</BODY>
</HTML>

