<?php

interface ConverterServices
{
    public function getCurrency(): \Psr\Http\Message\ResponseInterface;

    public function checkNegativeAmount($amount): void;

    public function checkNonNumericAmount($amount): void;
}