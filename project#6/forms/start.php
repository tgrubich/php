<html>
<head>
</head>
<body background="https://st3.depositphotos.com/16527934/35592/i/950/depositphotos_355928984-stock-photo-bitcoin-currency-gray-background-digital.jpg?forcejpeg=true">
<form align="center" action="/forms/start.php" method="post">
    <div id="box">
        <h1>
            <center>Currency Converter</center>
        </h1>
        <table align="center">
            <tr>
                <td>
                    <h3>Enter Amount:<input type="text" name="amount"></h3><br>
                </td>
            </tr>
            <tr>
                <td>
                    <br>
                    <h3><center>Currency:<select name='cur1'>
                            <option value="AUD">Австралийский доллар(AUD)</option>
                            <option value="AZN" selected>Азербайджанский манат(AZN)</option>
                            <option value="GBP" selected>Фунт стерлингов Соединенного королевства(GBP)</option>
                            <option value="AMD" selected>Армянских драмов(AMD)</option>
                            <option value="BYN" selected>Белорусский рубль(BYN)</option>
                            <option value="BGN" selected>Болгарский лев(BGN)</option>
                            <option value="BRL" selected>Бразильский реал(BRL)</option>
                            <option value="HUF" selected>Венгерских форинтов(HUF)</option>
                            <option value="HKD" selected>Гонконгских долларов(HKD)</option>
                            <option value="DKK" selected>Датских крон(DKK)</option>
                            <option value="USD" selected>Доллар США(USD)</option>
                            <option value="EUR" selected>Евро(EUR)</option>
                            <option value="INR" selected>Индийских рупий(INR)</option>
                            <option value="KZT" selected>Казахстанских тенге(KZT)</option>
                            <option value="CAD" selected>Канадский доллар(CAD)</option>
                            <option value="KGS" selected>Киргизских сомов(KGS)</option>
                            <option value="CNY" selected>Китайских юаней(CNY)</option>
                            <option value="MDL" selected>Молдавских леев(MDL)</option>
                            <option value="NOK" selected>Норвежских крон(NOK)</option>
                            <option value="PLN" selected>Польский злотый(PLN)</option>
                            <option value="RON" selected>Румынский лей(RON)</option>
                            <option value="XDR" selected>СДР (специальные права заимствования)(XDR)</option>
                            <option value="SGD" selected>Сингапурский доллар(SGD)</option>
                            <option value="TJS" selected>Таджикских сомони(TJS)</option>
                            <option value="TRY" selected>Турецких лир(TRY)</option>
                            <option value="TMT" selected>Новый туркменский манат(TMT)</option>
                            <option value="UZS" selected>Узбекских сумов(UZS)</option>
                            <option value="UAH" selected>Украинских гривен(UAH)</option>
                            <option value="CZK" selected>Чешских крон(CZK)</option>
                            <option value="SEK" selected>Шведских крон(SEK)</option>
                            <option value="CHF" selected>Швейцарский франк(CHF)</option>
                            <option value="ZAR" selected>Южноафриканских рэндов(ZAR)</option>
                            <option value="KRW" selected>Вон Республики Корея(KRW)</option>
                            <option value="JPY" selected>Японских иен(JPY)</option>
                        </select></h3>
                </td>
            </tr>
            <tr>
                <td>
                    <center><br>
                        <input type='submit' name='submit' value="Convert"></center>
                </td>
            </tr>
        </table>
</form>
<?php

require_once '../vendor/autoload.php';
require_once '../models/MyException.php';
require_once '../models/Currency.php';
require_once '../models/Converter.php';

use App\Exceptions\MyException;

if (isset($_POST['submit'])) {

    $amount = $_POST['amount'];
    $cur1 = $_POST['cur1'];

    $currency = new Currency();
    $converter = new Converter();

    try {
        $response = $currency->getCurrency();
        $currency->checkNegativeAmount($amount);

        if ($response->getStatusCode() == 200) {
            $json = $response->getBody();
            $yummy = json_decode($json);

            $currency->checkNonNumericAmount($amount);
            echo "<center><b>Your Converted Amount is:</b><br></center>";
            $res = $converter->currencyConvert($yummy, $amount, $cur1);
            if ($res > 0)
                echo "<center>" . $res . "</center>";
            else {
                $currency->checkNegativeAmount($res);
            }
        }
    } catch (\App\Exceptions\MyException $e) {
        echo $e->getMessage();
        echo "\n";
        echo "; status code: ";
        echo $e->getCode();
    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
    }
}
?>
</body>
</html>