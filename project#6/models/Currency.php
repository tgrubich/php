<?php

use App\Exceptions\MyException;
use GuzzleHttp\Client;

require_once '../services/ConverterServices.php';

class Currency implements ConverterServices
{

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCurrency(): \Psr\Http\Message\ResponseInterface
    {
        $client = new Client([
            'base_uri' => 'https://www.cbr-xml-daily.ru/daily_json.js',
        ]);

        $response = $client->request('GET', 'https://www.cbr-xml-daily.ru/daily_json.js', [
            'query' => [
                'base' => 'https://www.cbr-xml-daily.ru/daily_json.js',
            ]
        ]);
        return ($response);
    }

    public function checkNegativeAmount($amount): void
    {
        if ($amount < 0) {
            throw new MyException('Negative number entered', 406);
        }
    }

    public function checkNonNumericAmount($amount): void
    {
        if (!(is_numeric($amount))) {
            throw new MyException('Non-numeric value entered', 406);
        }
    }
}