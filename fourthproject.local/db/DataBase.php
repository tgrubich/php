<?php

class DataBase
{
    public function getConnection()
    {
        $db = pg_connect("host=localhost port=5433 dbname=secondDB user=postgres password=000");
        if (pg_connection_status($db) !== PGSQL_CONNECTION_OK) {
            return die("error connection");
        } else {
            return $db;
        }
    }
}