<?php

interface UserServices
{
    public function addUser($login, $password);

    public function findByLogin($login, $password);
}