<?php
require_once '../db/DataBase.php';
include '../services/UserServices.php';
include '../models/PrintState.php';

class User extends PrintState implements UserServices
{
    public $login;
    public $password;

    public function addUser($login, $password)
    {
        $db = new DataBase();
        $db = $db->getConnection();

        $sqlInsert = "INSERT INTO users (login, password) VALUES ('$login', '$password')";
        $sql = pg_query($db, $sqlInsert);
        if ($sql) {
            header("Location: /forms/index_first.php");
        } else {
            header("Location: /forms/save.php");
        }
    }

    public function findByLogin($login, $password)
    {
        $db = new DataBase();
        $db = $db->getConnection();

        $state = new User();

        $sqlSelect = "SELECT id FROM users WHERE login='$login'";

        $result = pg_exec($db, $sqlSelect);
        $myrow = pg_numrows($result);
        for ($i = 0; $i < $myrow; $i++) {
            $row = pg_fetch_array($result, $i);
            echo $row["login"];
        }
        if ($myrow != 0) {
            exit ($state->getState());
        } else {
            $this->addUser($login, $password);
        }
    }

    protected function getState()
    {
        $state = "Login already registered. Choose another login!!";
        return $state;
    }
}