<?php

require_once 'models/Sort.php';
require_once 'models/Search.php';
require_once 'models/SortUtils.php';
require_once 'models/SearchUtils.php';

$print = new SortUtils();
$send = new SearchUtils();

$N = $print->generateCountNumbers();
$array[$N] = 0;
for ($i = 0; $i < $N; $i++) {
    $array[$i] = mt_rand(-100,100);
}
echo 'Sorting methods for massive:';
echo ' </br>';
$print->testBubbleSort($array);
$print->testInsertSort($array);
$print->testMergerSort($array);
$print->testQuickSort($array);
$print->testSelectSort($array);

echo 'Search methods for massive:';
echo ' </br>';
echo 'Founding number 2';
echo ' </br>';
$mass = array(0, 3, 95, 2, -100, 4, 17, 81, 1, 7);
$send->testSequentialSearch($mass, 2);
$send->testIndexSequentialSearch($mass, 2);
$send->testBinarySearch($mass, 2);
