<?php

interface SortServices
{
    public function startBubbleSort($mass): array;

    public function startInsertSort($mass): array;

    public function startMergerSort($mass): array;

    public function mergerSort($mass1, $mass2): array;

    public function startQuickSort($mass): array;

    public function startSelectSort($mass): array;
}