<?php

interface SearchServices
{
    public function startSequentialSearch($mass, $index): int;

    public function startIndexSequentialSearch($mass, $index): int;

    public function startBinarySearch($mass, $index): int;
}