<?php

require_once 'services/SortServices.php';

class Sort implements SortServices
{

    public function startBubbleSort($mass): array
    {
        for ($i = 0; $i < count($mass); $i++)
        {
            for ($j = $i + 1; $j < count($mass); $j++)
            {
                if ($mass[$i] > $mass[$j])
                {
                    $temp = $mass[$i];
                    $mass[$i] = $mass[$j];
                    $mass[$j] = $temp;
                }
            }
        }
        return $mass;
    }

    public function startInsertSort($mass): array
    {
        for ($i = 0; $i < count($mass); $i++)
        {
            $temp = $mass[$i];
            $j = $i - 1;

            while (isset($mass[$j]) && $mass[$j] > $temp)
            {
                $mass[$j + 1] = $mass[$j];
                $mass[$j] = $temp;
                $j--;
            }
        }
        return $mass;
    }

    public function startMergerSort($mass): array
    {
        $count = count($mass);
        if ($count <= 1) {
            return $mass;
        }

        $firstMass  = array_slice($mass, 0, (int)($count/2));
        $secondMass = array_slice($mass, (int)($count/2));

        $firstMass = $this->startMergerSort($firstMass);
        $secondMass = $this->startMergerSort($secondMass);
        return $this->mergerSort($firstMass, $secondMass);
    }

    public function mergerSort($mass1, $mass2): array
    {
        $mass = array();

        while (count($mass1) > 0 && count($mass2) > 0) {
            if ($mass1[0] < $mass2[0]) {
                $mass[] = array_shift($mass1);
            } else {
                $mass[] = array_shift($mass2);
            }
        }

        array_splice($mass, count($mass), 0, $mass1);
        array_splice($mass, count($mass), 0, $mass2);

        return $mass;
    }

    public function startQuickSort($mass): array
    {
        $firstPart = $secondPart = array();
        if(count($mass) < 2)
        {
            return $mass;
        }
        $zeroElement = key($mass);
        $pivot = array_shift($mass);
        foreach($mass as $val)
        {
            if($val <= $pivot)
            {
                $firstPart[] = $val;
            }else if ($val > $pivot)
            {
                $secondPart[] = $val;
            }
        }
        return array_merge($this->startQuickSort($firstPart),array($zeroElement=>$pivot), $this->startQuickSort($secondPart));
    }

    public function startSelectSort($mass): array
    {
        for ($i = 0; $i < count($mass) - 1; $i++)
        {
            $min = $i;

            for ($j = $i + 1; $j < count($mass); $j++)
            {
                if ($mass[$j] < $mass[$min])
                {
                    $min = $j;
                }
            }

            $temp = $mass[$i];
            $mass[$i] = $mass[$min];
            $mass[$min] = $temp;
        }
        return $mass;
    }
}