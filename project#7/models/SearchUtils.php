<?php

require_once 'Search.php';
require_once 'SortUtils.php';

class SearchUtils
{
    public function testSequentialSearch($mass, $index)
    {
        $print = new SortUtils();
        $send = new Search();

        echo 'Previous massive:';
        echo ' </br>';
        $print->printResult($mass);
        echo ' </br>';
        echo 'Founding number has index (SequentialSearch):';
        echo ' </br>';
        $time_start = microtime(true);
        $end = $send->startSequentialSearch($mass, $index);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $end;
        echo ' </br>';
        echo "Search method running time (SequentialSearch): $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testIndexSequentialSearch($mass, $index)
    {
        $send = new Search();

        echo 'Founding number has index (IndexSequentialSearch):';
        echo ' </br>';
        $time_start = microtime(true);
        $end = $send->startIndexSequentialSearch($mass, $index);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $end;
        echo ' </br>';
        echo "Search method running time (IndexSequentialSearch): $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testBinarySearch($mass, $index)
    {
        $send = new Search();

        echo 'Founding number has index (BinarySearch):';
        echo ' </br>';
        $time_start = microtime(true);
        $end = $send->startBinarySearch($mass, $index);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $end;
        echo ' </br>';
        echo "Search method running time (BinarySearch): $time";
        echo ' </br>';
        echo ' </br>';
    }
}