<?php

require_once 'Sort.php';

class SortUtils
{
    public function printResult($result)
    {
        for ($i = 0; $i < count($result); $i++)
        {
            echo '<pre>';
            echo '[';
            echo $i;
            echo '] =>';
            print_r($result[$i]);
            echo '</pre>';
        }

    }

    public function generateCountNumbers(): int
    {
        return mt_rand(3, 10);
    }

    public function testBubbleSort($array)
    {
        $sort = new Sort();
        $print = new SortUtils();

        echo 'Previous massive:';
        echo ' </br>';
        $print->printResult($array);
        echo ' </br>';
        echo 'Massive after bubble sorting:';
        echo ' </br>';
        $time_start = microtime(true);
        $result = $sort->startBubbleSort($array);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $print->printResult($result);
        echo "Algorithm running time: $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testInsertSort($array)
    {
        $sort = new Sort();
        $print = new SortUtils();

        echo 'Massive after insert sorting:';
        echo ' </br>';
        $time_start = microtime(true);
        $result = $sort->startInsertSort($array);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $print->printResult($result);
        echo "Algorithm running time: $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testMergerSort($array)
    {
        $sort = new Sort();
        $print = new SortUtils();

        echo 'Massive after merger sorting:';
        echo ' </br>';
        $time_start = microtime(true);
        $result = $sort->startMergerSort($array);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $print->printResult($result);
        echo "Algorithm running time: $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testQuickSort($array)
    {
        $sort = new Sort();
        $print = new SortUtils();

        echo 'Massive after quick sorting:';
        echo ' </br>';
        $time_start = microtime(true);
        $result = $sort->startQuickSort($array);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $print->printResult($result);
        echo "Algorithm running time: $time";
        echo ' </br>';
        echo ' </br>';
    }

    public function testSelectSort($array)
    {
        $sort = new Sort();
        $print = new SortUtils();

        echo 'Massive after select sorting:';
        echo ' </br>';
        $time_start = microtime(true);
        $result = $sort->startSelectSort($array);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $print->printResult($result);
        echo "Algorithm running time: $time";
        echo ' </br>';
        echo ' </br>';
    }
}