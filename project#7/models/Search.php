<?php

require_once 'services/SearchServices.php';
require_once 'models/Sort.php';

class Search implements SearchServices
{

    public function startSequentialSearch($mass, $index): int
    {
        for ($i = 0; $i < count($mass); $i++)
        {
            if ($mass[$i] == $index)
                return $i;
        }
        return 0;
    }

    public function startIndexSequentialSearch($mass, $index): int
    {
        $result = new Sort();
        $key_index_mass = array();
        $current_index_mass = array();
        $mass = $result->startBubbleSort($mass);
        for ($i = 0, $j = 0; $i < count($mass); $i = $i + 8, $j++)
        {
            $key_index_mass[$j] = $mass[$i];
            $current_index_mass[$j] = $i;
        }
        $last_j = $j;
        $current_index_mass[$j] = count($mass);
        for ($j = 0; $j < $last_j; $j++)
        {
            if ($index < $key_index_mass[$j])
                break;
        }
        if ($j == 0)
            $i = 0;
        else
        {
            $i = $current_index_mass[$j - 1];
        }
        for ($i; $i < $current_index_mass[$j]; $i++)
        {
            if ($mass[$i] == $index)
                return $i;
        }
        return 0;
    }

    public function startBinarySearch($mass, $index): int
    {
        $low = 0;
        $high = count($mass) - 1;
        $sort = new Sort();

        $mass = $sort->startBubbleSort($mass);

        while ($low <= $high)
        {
            $middle_element = ($low + $high) / 2;
            if ($mass[$middle_element] > $index)
                $high = $middle_element - 1;
            else if ($mass[$middle_element] < $index)
                $low = $middle_element + 1;
            else
                return $middle_element;
        }
        return 0;
    }
}